

(function()
{

$( document ).ready(function() {



//first-section
var controller = new ScrollMagic.Controller({});

//left-images
new ScrollMagic.Scene({
    offset: 0
  })
    .setVelocity("#left-image", {
        translateX: '0px',
      },{
        duration: 1000,
      })
    .addTo(controller);

//right-images
new ScrollMagic.Scene({
    offset: 0
  })
    .setVelocity("#right-image", {
        translateX: '0',
      },{
        duration: 1000,
      })
    .addTo(controller);


//whenscroll offset 50


new ScrollMagic.Scene({
    offset: 50
  })
    .setVelocity("#second-logo", {
        scale: 1,
        opacity: 1
      },{
        delay: 500,
        duration: 500,
      })
    .addTo(controller);

//left-images
new ScrollMagic.Scene({
    offset: 50
  })
    .setVelocity("#left-image", {
        translateX: '-130%',
      },{
        duration: 1000,
      })
    .addTo(controller);

//right-images
new ScrollMagic.Scene({
    offset: 50
  })
    .setVelocity("#right-image", {
        translateX: '130%',
      },{
        duration: 1000,
      })
    .addTo(controller);


new ScrollMagic.Scene({
    offset: 50
  })
    .setVelocity("#center-image", {
        top: '-220%',
      },{
        duration: 500,
      })
    .addTo(controller);


new ScrollMagic.Scene({
    offset: 50
  })
    .setVelocity("#logo", {
        opacity: 0,

      },{
        delay: 500,
        duration: 500,
      })
    .addTo(controller);

new ScrollMagic.Scene({
    offset: 50
  })
    .setVelocity("#first-section", {
        opacity: 0,
      },{
        duration: 500,
      })
    .setClassToggle("#first-section", "remove-zindex")
    .addTo(controller);


//set class first animation
new ScrollMagic.Scene({
    offset: 50
  })
    .setClassToggle("#first-left", "rotateInUpRight")
    .addTo(controller);

//set class first animation
new ScrollMagic.Scene({
    offset: 50
  })
    .setClassToggle("#first-right", "rotateInUpLeft")
    .addTo(controller);


//set all up animation

// singapore ID animation
   new ScrollMagic.Scene({
    triggerElement: "#third_section",
    offset: 0
  })
    .setClassToggle('.upanim', "fadeInUpBig")
    .addTo(controller);

// Cantonese ID animation
   new ScrollMagic.Scene({
    triggerElement: "#canton_section",
    offset: 0
  })
    .setClassToggle('.upanim2', "fadeInUpBig")
    .addTo(controller);

// Chinese ID animation
   new ScrollMagic.Scene({
    triggerElement: "#chinese_section",
    offset: 0
  })
    .setClassToggle('.upanim3', "fadeInUpBig")
    .addTo(controller);


// India ID animation
   new ScrollMagic.Scene({
    triggerElement: "#india_section",
    offset: 0
  })
    .setClassToggle('.upanim4', "fadeInUpBig")
    .addTo(controller);





//add navigator function
  // build scenes
  var duration = $(window).height();
  duration -= 50;
  var controller = new ScrollMagic.Controller({globalSceneOptions: {duration: duration}});

  new ScrollMagic.Scene({triggerElement: "#singapore_id"})
          .setClassToggle("#singapore", "active") // add class toggle
          .addTo(controller);
  new ScrollMagic.Scene({triggerElement: "#chinese"})
          .setClassToggle("#cn", "active") // add class toggle
          .addTo(controller);
  new ScrollMagic.Scene({triggerElement: "#cantonese"})
          .setClassToggle("#ct", "active") // add class toggle
          .addTo(controller);
  new ScrollMagic.Scene({triggerElement: "#india"})
          .setClassToggle("#idn", "active") // add class toggle
          .addTo(controller);
  new ScrollMagic.Scene({triggerElement: "#apac_id"})
          .setClassToggle("#apac", "active") // add class toggle
          .addTo(controller);




//nav controller
// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length)
      {
        var window_height = $(window).height();
        var target_offset = target.offset().top;
        var target_section = target.parents( '.section_third_content' );
        var target_section_height = target_section.height();
        if ( window_height > target_section_height ) {
            target_offset -= (window_height - target_section_height)/2;
        } else {
            target_offset -= 30;
        }

        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target_offset
        }, 2000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });



// Helper function for add element box list in WOW
  WOW.prototype.addBox = function(element) {
    this.boxes.push(element);
  };

  // Init WOW.js and get instance
  var wow = new WOW();
  wow.init();

  // Attach scrollSpy to .wow elements for detect view exit events,
  // then reset elements and add again for animation
  $('.wow').on('scrollSpy:exit', function() {
    $(this).css({
      'visibility': 'hidden',
      'animation-name': 'none'
    }).removeClass('animated');
    wow.addBox(this);
  }).scrollSpy();
});








})();
