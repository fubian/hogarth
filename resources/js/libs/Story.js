//
// Story
//
import _ from 'lodash';

class Story
{
    /**
     * Class constructor
     *
     * @return void
     */
    constructor()
    {
        this.container = $( '.story--experience' );
        this.slides = $( '._slide', this.container );
        this.slideCount = this.slides.length;
        this.slideSpeed = 800;
        this.currentIndex = 1;
        this.sliderControl = false;
        this.isMobile = false;
    }

    /**
     * Setup
     *
     * @return mixed
     */
    setup()
    {
        let self = this;

        if ( $( '.story--experience' ).length > 0 )
        {
            //
            // Slider control
            //
            $('.story--experience input[type="range"]').ionRangeSlider({
                min: 1,
                max: self.slideCount,
                step: 1,
                onStart: function (data) {
                    self.slideOnStart( data );
                },
                onChange: function (data) {
                    self.slideOnChange( data );
                },
                onFinish: function (data) {
                    self.slideOnFinish( data );
                },
                onUpdate: function (data) {
                    self.slideOnUpdate( data );
                }
            });
            this.sliderControl = $('.story--experience input[type="range"]').data("ionRangeSlider");

            //
            // Setup env
            //
            this.setupEnv();

            //
            // Setup content
            //
            this.setupContent();

            //
            //Resize
            //
            this.resize();

            //
            //Setup swipe
            //
            this.swipe();
        }
    }

    /**
     * Setup resize
     *
     * @return mixed
     */
    resize()
    {
        let self = this;
        $(window).resize(function()
        {
            self.setupContent();
        });
    }

    /**
     * Setup env
     *
     * @return mixed
     */
    setupEnv()
    {
        $( '._experience--slide img' ).each( function()
        {
            let imgobj = $(this).get(0);
            imgobj.addEventListener( 'drag', function(e){
                e.preventDefault();
                return false;
            });
            imgobj.addEventListener( 'dragstart', function(e){
                e.preventDefault();
                return false;
            });
        })
    }

    /**
     * Setup content
     *
     * @return mixed
     */
    setupContent()
    {
        let winWidth = $(window).width();
        let header = $( '._experience--header', this.container );
        let headerWidth = header.width();

        if ( winWidth != headerWidth )
        {
            var contentWidth = winWidth - headerWidth;
            var slideWidth = 0.4 * contentWidth;
            var activeWidth = 0.6 * contentWidth;
        }
        else
        {
            var contentWidth = winWidth;
            var slideWidth = contentWidth;
            var activeWidth = contentWidth;
            this.isMobile = true;
        }
        let slidesWidth = (this.slideCount - 1) * slideWidth;
        let allWidth = slidesWidth + activeWidth;

        $( '._experience--slide ._col--inner', this.container ).width( allWidth );
        $( '._slide', this.container ).width( slideWidth );
        $( '._slide--active', this.container ).width( activeWidth );

        this.slideWidth = slideWidth;
        this.slidesWidth = slidesWidth;
        this.activeWidth = activeWidth;

        if ( ! this.isMobile ){
            this.contentHeight = $( '._slide--active', this.container ).height();

            this.container.height( this.contentHeight );
            $( '._slide', this.container ).height( this.contentHeight );
            $( '._experience--slide ._col--inner', this.container ).height( this.contentHeight );
        }
    }

    /**
     * Swipe control
     *
     * @return void
     */
    swipe()
    {
        let self = this;
        let slider, sliderLeft;
        let winWidth = $(window).width();

        new Hammer( $('._experience--slide')[0], {
            domEvents: true
        });

        $('._experience--slide').on( "panstart", function( e )
        {
            slider = $( "._experience--slide ._col--inner" );
            sliderLeft = parseInt( slider.css( "marginLeft" ), 10 );
            slider.stop().css({marginLeft:0});
        });

        $('._experience--slide').on( "panend", function( e )
        {
            let left = parseInt( slider.css( "marginLeft" ), 10 );
            left = Math.abs( left );

            let slice = left/self.slideWidth;
            let prevIndex = Math.floor( slice );
            let nextIndex = Math.ceil( slice );

            let halfWidth = self.width/2;
            let prevWidth = prevIndex * self.width;
            let nextWidth = nextIndex * self.width;
            let nextHalf = prevWidth + halfWidth;
            let goindex = 1;

            if ( left > nextHalf ) {
                self.goto( nextIndex + 1, true );
                goindex = nextIndex + 1;
            } else {
                self.goto( prevIndex + 1, true );
                goindex = prevIndex + 1;
            }

            self.sliderControl.update({
                from: goindex
            });
        });

        $('._experience--slide').on( "pan", function( e )
        {
            if ( self.slidesWidth > winWidth )
            {
                var maxLeft = (self.slidesWidth - winWidth) + self.slideWidth;
                var delta = sliderLeft + e.originalEvent.gesture.deltaX;
                if ( delta >= (-1*maxLeft) && delta <= 0 ) {
                   slider.css( {
                    "marginLeft": delta
                  });
                }
            }
        });
    }

    /**
     * Go to target slider
     *
     * @param int index
     * @return mixed
     */
    goto( index, forced = false )
    {
        if ( ! forced && index == this.currentIndex ) {
            return false;
        }
        let current = $( '._slide:nth-child('+this.currentIndex+')', this.container );
        let active = $( '._slide:nth-child('+index+')', this.container );

        if ( active.length > 0 )
        {
            $( '._slide', this.container ).removeClass( '_slide--active' );
            active.addClass( '_slide--active' );

            let toLeft = (index - 1) * this.slideWidth * -1;
            let wrapper = $( '._experience--slide ._col--inner', this.container );
            wrapper.animate({
                marginLeft: toLeft
            },
            this.slideSpeed );

            if ( ! this.isMobile )
            {
                $( '._slide--image', current ).height( this.contentHeight );
                $( 'img', current ).height( this.contentHeight );
                $( '._slide--image', current ).animate({
                    height: 270
                },
                this.slideSpeed );
                $( 'img', current ).animate({
                    height: 'auto'
                },
                this.slideSpeed );
                current.animate({
                    width: this.slideWidth
                },
                this.slideSpeed );

                $( '._slide--image', active ).animate({
                    height: this.contentHeight
                },
                this.slideSpeed );
                $( 'img', active ).height( 'auto' );
                $( 'img', active ).animate({
                    height: this.contentHeight
                },
                this.slideSpeed );
                active.animate({
                    width: this.activeWidth
                },
                this.slideSpeed );
            }

            //set data
            this.currentIndex = index;

        }
    }

    /**
     * event: slideOnStart
     *
     * @param object data
     * @return mixed
     */
    slideOnStart( data )
    {
    }

    /**
     * event: slideOnChange
     *
     * @param object data
     * @return mixed
     */
    slideOnChange( data )
    {
    }

    /**
     * event: slideOnFinish
     *
     * @param object data
     * @return mixed
     */
    slideOnFinish( data )
    {
        if ( data.from == this.currentIndex ) {
            return false;
        }

        let index = parseInt( data.from );
        this.goto( index );
    }

    /**
     * event: slideOnUpdate
     *
     * @param object data
     * @return mixed
     */
    slideOnUpdate( data )
    {
    }
}
export default Story;
