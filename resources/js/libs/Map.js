//
// Map
//
import $ from 'jquery';
import _ from 'lodash';
import TBSection from "../libs/general/Ui/Toggle/Section";
import perfectScrollbar from 'perfect-scrollbar';

class Map
{
    /**
     * Class constructor
     *
     * @return void
     */
    constructor( location )
    {
        this.map = false;
        this.mapData = {};
        this.mapMarkers = [];
        this.locationData = location;
        this.currentZoom = 12;
        this.scrollbar = $( '.location--info--detail ._store--list ._scrollbar' );
    }

    /**
     * Setup
     *
     * @return mixed
     */
    setup()
    {
        //Init size
        this.initSize();

        //Init map
        this.initMap();

        //Init filter area
        this.initFilterArea();

        //Init Auto complete
        this.initAutocomplete();

        //Init Current Location
        this.initCurrentLocation();

        //Init actions
        this.initActions();

        // Section
        this.detailSection = new TBSection().setup();

        //close detail
        let self = this;
        $( '.location--info--detail ._close' ).click( function()
        {
            self.detailSection.closeAnimation();
            return false;
        });
        $( '.location--info--detail ._filter--back' ).click( function()
        {
            self.detailSection.closeAnimation();
            return false;
        });

        //scrollbar
        let cont = this.scrollbar.get(0);
        perfectScrollbar.initialize( cont, {
            suppressScrollX: true,
            minScrollbarLength: 68
        });

        return this;
    }

    /**
     * Init size
     *
     * @return void
     */
    initSize()
    {
        //Get map size
        let winHeight = $(window).height();
        let headerHeight = $( '.header.desktop' ).outerHeight();
        let footerHeight = $( '.footer.desktop' ).outerHeight();
        let mapHeight = winHeight - headerHeight - footerHeight;
        $( '.location--main' ).height( mapHeight );

        //relocation info
        let infoEl = $( '.location--info' );
        let infoHeight = infoEl.outerHeight();

        if ( infoHeight > ( mapHeight - 20 ) )
        {
            let infoHeight = mapHeight - 20;
            infoEl.height( infoHeight );
            infoEl.css( 'top', '10px' );
        }
        else
        {
            let toTop = mapHeight/2 - infoHeight/2;
            infoEl.css( 'top', toTop+'px' );
        }

        //get height info detail scroll
        let paddingV = 30;
        let detailTitleHeight = $( '.location--info--detail ._detail--header' ).outerHeight();
        let scrollHeight = infoHeight - paddingV - detailTitleHeight;
        $( '.location--info--detail ._store--list' ).height( scrollHeight );
    }

    /**
     * Init filter area
     *
     * @return void
     */
    initActions()
    {
        let self = this;
        $( '.location--map--actions ._action--plus' ).click( function()
        {
            let zoom = self.currentZoom;
            if ( zoom == 22 ) {
                return false;
            }
            zoom++;
            self.map.setZoom( zoom );
            self.currentZoom = zoom;
            return false;
        });
        $( '.location--map--actions ._action--minus' ).click( function()
        {
            let zoom = self.currentZoom;
            if ( zoom == 0 ) {
                return false;
            }
            zoom--;
            self.map.setZoom( zoom );
            self.currentZoom = zoom;
            return false;
        });
    }

    /**
     * Init filter area
     *
     * @return void
     */
    initCurrentLocation()
    {
        let self = this;
        $( '.location--current ._address--value' ).click( function()
        {
            let button = $(this);
            let lng = parseFloat( button.attr( 'data-lng' ) );
            let lat = parseFloat( button.attr( 'data-lat' ) );
            let id  = button.attr( 'data-id' );

            if ( typeof lng != 'undefined' && typeof lat != 'undefined' )
            {
                self.map.setCenter({
                    "lat": lat,
                    "lng": lng
                });

                if ( typeof id != 'undefined' )
                {
                    let storeData = self.mapData[id];
                    if ( typeof storeData != 'undefined' )
                    {
                        self.infowindow.setContent( storeData.info );
                        self.infowindow.open( self.map, storeData.marker );
                    }
                }
            }

            return false;
        });
    }

    /**
     * Init filter area
     *
     * @return void
     */
    initFilterArea()
    {
        let self = this;
        $( '.button._area--menu' ).click( function()
        {

            $( '.button._area--menu' ).removeClass( '_active' );
            $(this).addClass( '_active' );

            let areaName = $(this).attr( 'href' ).replace( '#area--', '' );

            if ( typeof self.locationData['area'][areaName] != 'undefined' )
            {
                let area = self.locationData['area'][areaName];
                self.addStoreMarkers( area.stores );

                $( '.location--current ._address--value' )
                    .text( area.title )
                    .attr( 'data-lng', area.center.lng )
                    .attr( 'data-lat', area.center.lat );

                self.map.setCenter( area.center );

                //set detail title
                $( '.location--info--detail ._title' ).text( areaName );

                //Render store list
                self.scrollbar.html( '' );
                if ( area.stores.length > 0 )
                {
                    for( let i = 0; i < area.stores.length; i++ )
                    {
                        let store = area.stores[i];
                        let gmLink = 'https://www.google.com/maps/search/?api=1&query='+store.map.lat+','+store.map.lng;
                        let item =
                            $( '<div></div>')
                                .addClass( '_store' )
                                .attr( 'data-id', store.id )
                                .attr( 'data-lat', store.map.lat )
                                .attr( 'data-lng', store.map.lng )
                                .attr( 'data-address', store.address.replace(/(<([^>]+)>)/ig,"") )
                                .append(
                                    $( '<div></div>')
                                        .addClass( '_store--inner' )
                                        .append(
                                            $( '<h3></h3>')
                                                .addClass( '_store--title' )
                                                .text( store.title )
                                        )
                                        .append(
                                            $( '<div></div>')
                                                .addClass( '_store--address' )
                                                .html( store.address.replace(/(<([^>]+)>)/ig,"") )
                                        )
                                        .append(
                                            $( '<div></div>')
                                                .addClass( '_store--info' )
                                                .text( store.operating + '/' + store.phone )
                                        )
                                )
                                .append(
                                    $( '<a></a>')
                                        .attr( 'href', gmLink )
                                        .attr( 'target', '_blank' )
                                        .addClass( '_store--link' )
                                );

                        item.appendTo( self.scrollbar );
                        $( '._store--inner', item ).click(function()
                        {
                            let parent = $(this).parents( '._store' );
                            let id = parseInt( parent.attr( 'data-id') );
                            let lat = parseFloat( parent.attr( 'data-lat' ) );
                            let lng = parseFloat( parent.attr( 'data-lng' ) );
                            let addr = parent.attr( 'data-address' );
                            if ( typeof lat != 'undefined' && typeof lng != 'undefined' )
                            {
                                self.map.setCenter({
                                    "lat": lat,
                                    "lng": lng
                                });

                                $( '.location--current ._address--value' )
                                    .text( addr )
                                    .attr( 'data-id', id )
                                    .attr( 'data-lng', lng )
                                    .attr( 'data-lat', lat );

                                let storeData = self.mapData[id];
                                if ( typeof storeData != 'undefined' )
                                {
                                    self.infowindow.setContent( storeData.info );
                                    self.infowindow.open( self.map, storeData.marker );
                                }
                            }
                            return false;
                        });
                    }
                }

                //Open detail
                self.detailSection.setHeight();
                self.detailSection.openAnimation();

            }
            return false;
        });
    }

    /**
     * Init autocomplete
     *
     * @return void
     */
    initAutocomplete()
    {
        let self = this;
        var inHoverAC = false;
        setTimeout(function()
        {
            $( '._keyword--select .autocomplete' ).addClass( 'init' );
            $( '._keyword--select .autocomplete' ).hover(function() {
                inHoverAC = true;
            }, function(){
                inHoverAC = false;
            });
        }, 200 );

        $.ajax({
           url: "data/location.json",
           type: 'GET',
           dataType: 'json',
           data: {},
           error: function() {
           },
           success: function( data )
           {
               if ( data && typeof data.area != 'undefined' )
               {
                   let locationsData = [];
                   for ( var key in data.area )
                   {
                       let area = data.area[key];
                       if ( typeof area.stores != 'undefined' )
                       {
                           for ( var i = 0; i < area.stores.length; i++ )
                           {
                               let store = area.stores[i];
                               let label = store.title;

                               locationsData.push({
                                   "value": key+'|'+store.id,
                                   "label": label
                               });
                           }

                       }
                   }


                    let acInput = document.getElementById( 'filter--keyword--input' );
                    new Awesomplete( acInput,
                    {
                        list: locationsData,
                        minChars: 1,
                        // filter: function (text, input) {
                        //     return text.indexOf(input) === 0;
                        // }
                        filter: Awesomplete.FILTER_STARTSWITH
                    });

                    acInput.addEventListener( 'awesomplete-select', function( ev )
                    {
                        if ( typeof ev.text != 'undefined' )
                        {
                            //Set input value
                            setTimeout( function(){
                                $( ev.target ).val( ev.text.label );
                            },50);

                            let id = ev.text.value;
                            let values = _.split( id, '|' );
                            let areaName = values[0];
                            let storeId = values[1];

                            let area = self.locationData.area[areaName];
                            if ( typeof area != 'undefined' )
                            {
                                //Reset markers
                                self.removeMarkers();
                                //add area markers
                                self.addStoreMarkers( area.stores );

                                //Get store
                                let store = false;
                                for ( var i = 0; i < area.stores.length; i++ )
                                {
                                    let current = area.stores[i];
                                    if ( current.id == storeId ) {
                                        store = current;
                                    }
                                }

                                if ( store )
                                {
                                    self.map.setCenter({
                                        "lat": store.map.lat,
                                        "lng": store.map.lng
                                    });

                                    $( '.location--current ._address--value' )
                                        .attr( 'data-lng', store.map.lng )
                                        .attr( 'data-lat', store.map.lat )
                                        .text( store.title );

                                    let storeData = self.mapData[storeId];
                                    if ( typeof storeData != 'undefined' )
                                    {
                                        self.infowindow.setContent( storeData.info );
                                        self.infowindow.open( self.map, storeData.marker );
                                    }

                                }
                            }
                        }
                    });
               }
           }
        });


    }

    /**
     * Init map
     *
     * @return void
     */
    initMap()
    {

        //Init map
        let mapStyle  = this.getMapStyle();
        let iconStyle = this.getMapIconDefault();
        let center = this.locationData['center'];

        let map = new google.maps.Map(document.querySelector('.location--map--view'), {
            disableDefaultUI: true,
            styles: mapStyle,
            zoom: 13,
            center: center
        });
        this.map = map;

        let infowindow = new google.maps.InfoWindow({
          content: '',
          maxWidth: 200
        });
        this.infowindow = infowindow;

        // *
        // START INFOWINDOW CUSTOMIZE.
        // *
        google.maps.event.addListener(infowindow, 'domready', function()
        {
          // Reference to the DIV that wraps the bottom of infowindow
          var iwOuter = $('.gm-style-iw');
          var iwWrap  = iwOuter.parent();
          iwWrap.addClass( 'gm-style-wrapper' );
          iwWrap.css( 'opacity', 0 );

          setTimeout( function()
          {
            iwWrap.addClass( '_animated' );
          },10);

          setTimeout( function()
          {
            $( '.gm-iw-root' ).css( 'opacity', 1 );
            iwWrap.addClass( '_open' );
          },50);

          /* Since this div is in a position prior to .gm-div style-iw.
           * We use jQuery and create a iwBackground variable,
           * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
          */
          var iwBackground = iwOuter.prev();

          // Arrow
          iwBackground.children(':nth-child(1)').addClass( 'box-arrow' );
          iwBackground.children(':nth-child(3)').hide();

          // Removes background shadow DIV
          iwBackground.children(':nth-child(2)').css({'display' : 'none'});

          // Removes white background DIV
          iwBackground.children(':nth-child(4)').css({'display' : 'none'});

          // Reference to the div that groups the close button elements.
          var iwCloseBtn = iwOuter.next();

          // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
          if($('.iw-content').height() < 140){
            $('.iw-bottom-gradient').css({display: 'none'});
          }

          // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
          iwCloseBtn.mouseout(function(){
            $(this).css({opacity: '1'});
          });
        });

        this.addAllStores();

    }

    /**
     * Add all stores
     *
     * @return array
     */
    addAllStores()
    {
        let areas = this.locationData['area'];
        for ( let areaName in areas )
        {
            let area = areas[areaName];
            let stores = area.stores;
            this.addStoreMarkers( stores );

        }
    }

    /**
     * Add store marker
     *
     * @param object stores
     * @return object
     */
    addStoreMarkers( stores )
    {
        if ( typeof stores != 'undefined' && stores.length > 0 )
        {
            for( let i = 0; i < stores.length; i++ )
            {
                var store = stores[i];
                if ( store.map ) {
                    this.addMapMarker( store );
                }
            }
        }
    }

    /**
     * Add map marker
     *
     * @param object store
     * @return object
     */
    addMapMarker( store )
    {
        let self = this;
        let marker = new google.maps.Marker({
            position: store.map,
            map: this.map,
            icon: 'images/icon--map-pointer.png'
            //icon: this.getMapIconDefault()
        });

        //
        //Add Info Window
        //
        var info = '<div class="_store--info">' +
                      '<div class="_store--info--content">' +
                        '<p class="_store--info--name">' +
                            '<a href="https://www.google.com/maps/search/?api=1&query='+store.map.lat+','+store.map.lng+'" target="_blank">' +
                                store.title +
                            '</a>' +
                        '</p>' +
                        '<p class="_store--info--address">' + store.address + '</p>' +
                        '<p class="_store--info--open">' + store.operating + '</p>' +
                        '<p class="_store--info--phone">' + store.phone + '</p>' +
                      '</div>' +
                   '</div>';

        let storeId = store.id;
        this.mapData[storeId] = {
            store: store,
            marker: marker,
            info: info
        };
        this.mapMarkers.push( marker );

        marker.addListener( 'click', function()
        {
            self.infowindow.setContent( info );
            self.infowindow.open( self.map, marker );
        });

        return marker;
    }



    /**
     * Get map style
     *
     * @return array
     */
    getMapStyle()
    {
      let style = [{"featureType":"poi.business","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#bae6cf"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f9c8aa"}]},{"featureType":"transit","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#c6eced"}]}];

      return style;    }

    /**
     * Reset map markers
     *
     * @param int iconSize
     * @return mixed
     */
     removeMarkers()
     {
       if ( this.mapMarkers.length > 0 ) {
         for ( var i = 0; i < this.mapMarkers.length; i++ ) {
           this.mapMarkers[i].setMap( null );
         }
       }
       this.mapMarkers = [];
     }

    /**
     * Get map icon default
     *
     * @param int iconSize
     * @return mixed
     */
    getMapIconDefault( iconSize )
    {
        if ( typeof iconSize === 'undefined' ) {
            iconSize = 10;
        }
        let iconOptions = {
            path: google.maps.SymbolPath.CIRCLE,
            fillColor: '#9d2235',
            fillOpacity: .8,
            scale: iconSize,
            strokeColor: 'transparent',
            strokeWeight: .5
        };
        return iconOptions;
    }
};

export default Map;
