//
// UI helper
//
import $ from 'jquery';
import 'jquery.transit';
import Config from './../Config';

class Toggle
{
    /**
     * Class constructor
     *
     * @return void
     */
    constructor( settings = {} )
    {
        //set settings
        this._setSettings( settings );

        //get container
        this.containerId = this.config.get( 'containerClass' );
        this.container = new $( this.containerId );
    }

    /**
     * Setup toggle
     *
     * @return void
     */
    setup()
    {
        //prepare
        this.prepare();

        //trigger event
        this.trigger();
    }

    /**
     * Prepare toggle
     *
     * @return void
     */
    prepare()
    {

    }

    /**
     * Open event handler
     *
     * @return void
     */
    trigger()
    {
        let self = this;
        let container = this.container;
        let trigger = $( this.config.get( 'triggerClass' ), container );
        if ( trigger.length > 0 )
        {
            trigger.click( function()
            {
                //check class
                if ( container.hasClass( '_shown' ) ) {
                    self.close( $(this) );
                }
                else {
                    self.open( $(this) );
                }


                return false;
            });
        }
    }

    /**
     * Open toggle
     *
     * @return void
     */
    open()
    {
        //just add/remove class
        this.container.removeClass( '_hidden' );
        this.container.addClass( '_shown' );
    }

    /**
     * Close toggle
     *
     * @return void
     */
    close()
    {
        //just add/remove class
        this.container.removeClass( '_shown' );
        this.container.addClass( '_hidden' );
    }

    /**
     * Set settings
     *
     * @param array settings
     * @return Uploader
     */
    _setSettings( settings )
    {
        let defaultSettings = this._getDefaultSettings();
        this.config = new Config( defaultSettings, settings );
        return this;
    }

    /**
     * Get default settings
     *
     * @return object
     */
    _getDefaultSettings()
    {
        let settings = {
            containerClass: '.ui__toggle',
            triggerClass: '._trigger',
            detailClass: '._section--detail'
        };
        return settings;
    }

};

export default Toggle;
